/**
 * Der schnellste Wegfindungsalgorithmus. A-Stern ist ein informierter Algorithmus, weil er die Heuristik als Hilfsmittel miteinbezieht, sodass der Algorithmus immer ein Anhaltspunkt, wo der kürzeste Weg sich befindet. 
 */
public class AStern extends Algorithmus {

  // tatsächliches Kantengewicht + Heuristik
  private Map<Knoten, Float> laufkosten;
  // tatsächliches Kantengewicht
  private Map<Knoten, Float> realeKosten;


  public AStern(ArrayList<Knoten> _knoten) {
    super(_knoten);
  }

  /**
   * Startet den A-Stern-Algorithmus.
   * @param start Startpunkt
   * @param ziel Zielpunkt
   */
  @Override
  public void starteAlgorithmus(Knoten start, Knoten ziel) {
    // Messung der Laufzeit des Algorithmus
    startNanos = System.nanoTime();
    dauerNanos = System.nanoTime() - startNanos;
    // die benötigten Liste öffnen
    open = new ArrayList<Knoten>();
    closed = new ArrayList<Knoten>();
    finalerWeg = new ArrayList<Knoten>();

    this.laufkosten = new HashMap<Knoten, Float>();
    this.realeKosten = new HashMap<Knoten, Float>();
    vorgaenger = new HashMap<Knoten, Knoten>();

    // initialisiere Startknoten
    open.add(start);
    this.laufkosten.put(start, 0.0);
    this.realeKosten.put(start, 0.0);
    vorgaenger.put(start, null);

    while (open.size() > 0) {
      super.durchgang++;
      // ernenne den aktuellen Knoten mit dem kleinsten Kantengewicht
      Knoten aktuellerKnoten = minimalesKantengewicht(open, this.laufkosten);
      // setz die Heuristik für das aktuelle Element
      aktuellerKnoten.setzeHeuristik(this.hypotenuse(aktuellerKnoten.xKoordinate(), 
                                                         aktuellerKnoten.yKoordinate(), 
                                                         aktuellerKnoten.zKoordinate(), 
                                                         ziel.xKoordinate(), 
                                                         ziel.yKoordinate(), 
                                                         ziel.zKoordinate()));

      if (aktuellerKnoten == ziel) {
        // geht aus der Schleife heraus
        break;
      }

      closed.add(aktuellerKnoten);
      open.remove(aktuellerKnoten);

      this.sucheNachfolger(aktuellerKnoten, ziel);
    }

    wegErstellen(vorgaenger, ziel);
    System.out.println("\n\n" +
                       "A-Stern");
    statistikAusgeben();
    System.out.println("Dauer: " + dauerNanos + " ns");
  }

  /**
   * Ermittelt alle Nachbarn des aktuellen Knotens und speichert sie in die Open-Liste.
   * @param aktuellerKnoten Pivot-Punkt, um seine Nachbarn zu ermitteln
   * @param ziel Zielpunkt des Auswahls
   */
  @Override
  public void sucheNachfolger(Knoten aktuellerKnoten, Knoten ziel) {
    for (Knoten nachbar : aktuellerKnoten.listeMitNachbarn()) {
      if (closed.contains(nachbar)) {
        // geht zum nächsten Nachbarn
        continue;
      }

      // summiert die vorherigen Kantengewichte mit dem nächsten vom Nachbar
      float realeKantengewicht = this.realeKosten.get(aktuellerKnoten) + aktuellerKnoten.holeKantengewicht(nachbar);
      // berechnet die Heuristik vom aktuellen Knoten zum Zielknoten
      float heuristik = this.hypotenuse(aktuellerKnoten.xKoordinate(), 
                                            aktuellerKnoten.yKoordinate(), 
                                            aktuellerKnoten.zKoordinate(), 
                                            ziel.xKoordinate(), 
                                            ziel.yKoordinate(), 
                                            ziel.zKoordinate());
                                        
      aktuellerKnoten.setzeHeuristik(heuristik);

      float heuristikSumme = this.realeKosten.get(aktuellerKnoten) + aktuellerKnoten.holeKantengewicht(nachbar) + aktuellerKnoten.wertDerHeuristik();

      // wenn Nachbar nicht bereits in der Open-Liste drin ist
      if (!open.contains(nachbar)) {
        // dann füge ihn hinzu
        open.add(nachbar);
        // wiederhole den Vorgang, wenn die Kosten des nächsten Nachbarns größer ist
      } else if (heuristikSumme >= this.laufkosten.get(nachbar)) {
        continue;
      } else if (realeKantengewicht >= this.realeKosten.get(nachbar)) {
        continue;
      }

      vorgaenger.put(nachbar, aktuellerKnoten);
      this.laufkosten.put(nachbar, heuristikSumme);
      this.realeKosten.put(nachbar, realeKantengewicht);
    }
  }
    
  /**
   * Berechnet die längste Strecke zwischen Punkt A und Punkt B mit Hilfe vom Satz des Pythagoras.
   * Die Distanz vom startpunkt zum zielpunkt repräsentiert eine Kante  der Kathete.
   * Die Höhe ist dann die Gegenkathete.
   * @param startX  x-Koordinate vom Startpunkt
   * @param startY  y-Koordinate vom Startpunkt
   * @param startZ  z-Koordinate vom Startpunkt
   * @param zielX   x-Koordinate vom Zielpunkt
   * @param zielX   y-Koordinate vom Zielpunkt
   * @param zielX   z-Koordinate vom Zielpunkt
   */
  private float hypotenuse(float startX, float startY, float startZ, float zielX, float zielY, float zielZ) {
    return sqrt(pow(dist(startX, startY, zielX, zielY), 2) + pow(this.differenzHoeheZweierKnoten(startZ, zielZ), 2));
  }
  
  /**
   * Berechnet die Differenz der Höhe zweier Knoten. 
   * Mit der abs()-Funktion wird dabei den absoluten Wert (Vorzeichen unbehaftet) genommen.
   * @param startZ  Höhe des Startpunktes
   * @param zielZ   Höhe des Zielpunktes
   */
  private float differenzHoeheZweierKnoten(float startZ, float zielZ) {
   return abs(startZ - zielZ);
  }

  /**
   * Gibt alle möglichen Wege vom Start- zum Zielpunt mit ihren Kosten zurück.
   */
  @Override
  public void zurueckgelegteWegeAusgeben() {
    int bezeichnung = 0;

    for (Map.Entry<Knoten, Float> entry : this.realeKosten.entrySet()) {
      for (int i = 0; i < knoten.size(); i++) {
        if (knoten.get(i) == entry.getKey()) {
          bezeichnung = i;
        }
      }
      System.out.println("Knoten: " + bezeichnung + " | Kosten: " + entry.getValue());
    }
  }

  /**
   * Gibt die maximale Weglänge vom Start- zum Zielpunkt aus.
   * Eine Bedingung muss abgefangen werden, wenn es kein Weg vom Start- zum Zielpunkt existiert.
   * Dann entspricht die Gesamtkosten natürlich 0.
   * @return Die maximale Weglänge.
   */
  @Override
  public float maximaleWeglaengeAusgeben() {
    float zwischenGewicht = Float.MIN_VALUE;

    for (Knoten aktuellerKnoten : finalerWeg) {
      if (!this.realeKosten.containsKey(aktuellerKnoten)) {
        zwischenGewicht = 0.0;
      } else if (this.realeKosten.get(aktuellerKnoten) >= zwischenGewicht) {
        zwischenGewicht = this.realeKosten.get(aktuellerKnoten);
      }
    }
    return zwischenGewicht;
  }
} // ENDE Klasse AStern

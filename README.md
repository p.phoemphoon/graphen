# Höhenkarte
Der schnellste Weg von A nach B auf eine vordefinierte Höhenkarte. Es gibt 5 Karten zur Auswahl, die man mit vorbelegten Tasten auswählen kann.

![Map](resource/default-map.jpg)


## Voraussetzungen
Programmierung mit Processing 3.  
Eine lauffähige JDK Installation.  
Im Ordner **Application** befindet sich die laufähige Version für Windows 32-Bit und 64-Bit.

## Steuerung

### Auswahl der Karten
Die vordefinierten Karten lässt sich mit den Tasten **[1]**, **[2]**, **[3]**, **[4]** und **[5]** laden.  
Mit **[LEERTASTE]** wird die aktuelle Karte aktualisiert und auf dem Initialzustand versetzt.


### Start des Algorithmus

Um den Algorithmen zu starten, ist es vorausgesetzt, dass bereits ein Start- und Zielpunkt gesetzt wurde. Diese lässt sich beliebig an den Schnittpunkten im Raster setzen.  

    *Achtung: Setzt man ein Startpunkt im Wasser, so wird kein Weg zum Zielpunkt gefunden! Die Annahme ist nämlich, dass man nicht schwimmen kann.*  

![Auswahl](resource/selection.jpg)

Wurde bereits ein Start- und Zielpunkt gesetzt, dann hat man die Möglichkeit den Tasten  
**[a]** = A-Stern-Algorithmus,  
**[s]** = Zufalls-Algorithmus und  
**[d]** = Dijkstra-Algorithmus auszuwählen.  

Alle drei Algorithmen lässt sich sequenziell hintereinander ausführen, um beispielsweise die Laufzeit und den Gesamtweg miteinander zu vergleichen.  

![Pfad](resource/final-path.jpg)


---

## Changelog 
Alle Änderungen im Projekt werden hier im Dokument notiert.

### [Unreleased]
### Added



## [1.12.7] - 2019-02-06
### Added
- STABLE VERSION
- Renderer Klasse für die finale Version verworfen
    - resultiert unter anderem zu NullPointerException
    - die Farben sind nicht wartbar und selbstbeschreibend
- Code Clean up: unnötige Kommentare gelöscht


## [1.12.6] - 2019-02-05
### Added
- Potenzierung von Anstrengungsfaktor in Differenz zweier Höhen Methode umgeändert
- Zeitmessung in Nanosekunde bei allen Algorithmen hinzugefügt
- neuer Höhendaten hinzugefügt
    - [0] Big Map Karte wird geladen


## [1.11.6] - 2019-02-03
### Added
- Algorithmen sind auf folgenden Tasten belegt
    - [A] A-Stern-Algorithmus
    - [S] Zufallsalgorithmus
    - [D] Dijsktra-Algorithmus
- mit Leertaste [SPACE] wird die aktuelle Karte neugeneriert
- verschiedene Karte auf die Taste [1] - [5] zugewiesen


## [1.10.6] - 2019-02-02
### Added
- Wasserspiegel ist anpassbar, dementsprechend wird die Darstellung des Wassers auch angepasst
- Anstrengungsfaktor hinzugefügt
- Methoden hypotenuse() und differenzZweierKnoten() verallgemeinert in Klasse Graph
- Urspungshöhen auf ein Wert von 0 bis 100 gemappt


## [1.9.6] - 2019-01-28
### Added
- WIP: Code Refactoring, Java-Doc, Kommentare
- Bugfix: Berechnung in A-Stern: Die Höhe wird nun miteinbezogen.
- Ellipsen wird nun als Rechteck dargestellt.
- Die Karte ist nun an der Höhe farblich angepasst.
- Höhendaten aus Datei kann reingeladen und genutzt werden


## [1.8.6] - 2019-01-23
### Added
- WIP: Kantengewicht, die Differenz des Start- und Zielpunktes mit in der Berechnung einbeziehen
- Neuer Branch **alpha**: Gittergraph erstellt und Kanten werden angezeigt
- Refactoring: mousePressed(), Start- und Zielauswahl in Methoden ausgelagert
- Neue Funktion: Mehrfachauswahl des Start- und Endpunktes auf dem gleichen Feld ist jetzt möglich


## [1.7.6] - 2019-01-18
### Added
- WIP: Gittergraph
- WIP: Mehrfach Auswahl des Start- und Zielknotens auf der selben Karte
    - alte Auswahl soll entfernt werden
- Refactoring: erfolgreiches Mergen mit Auswahl des Start- und Zielknotens


## [1.6.6] - 2019-01-17
### Added
- WIP: Mergen mit Auswahl des Start- und Zielknotens
- Refactoring: Vererbung der Algorithmen
    - Klasse Algorithmus ist abstrakt
    - Zufallsweg-, Dijkstra- und A-Stern-Algorithmus sind Subklassen von Algorithmus
- grafische Darstellung des Highlights in Klasse Graph wurde ausgelagert


## [1.5.6] - 2019-01-16
### Added
- WIP: Vererbung der Algorithmen
- Kommentare in AStern hinzugefügt
- Refactoring: Code in allen Klassen
- Bugfix: A-Stern-Algorithmus, die Berechnung der Wege waren falsch


## [1.5.5] - 2019-01-14
### Added
- Mergen mit optimierte Dijkstra und zufälligen Algorithmus


## [1.5.4] - 2019-01-13
### Added
- Refactoring: EinWeg modifiziert zu Zufallsweg
- Refactoring: Dijkstra-Algorithmus optimiert
- die gesamte Weglänge wird auf die Konsole ausgegeben
- Bugfix: Ausfälle der Algorithmen
    - Lösung: Bei der Darstellung wurde der Weg zum Nachbarknoten nur von einer Seite erstellt
    - Bsp.
    - A -> B; Der Weg von B -> A fehlte


## [1.4.4] - 2018-12-19
### Added
- Bug: Wegfindung hat manchmal Ausfälle; der kürzeste, längste, zufälligen Weg wird manchmal oder gar nicht dargestellt
- Refactoring: neue Klasse Astern hinzugefügt


## [1.4.3] - 2018-12-17
### Added
- Bug: Wegfindung hat manchmal Ausfälle; der kürzeste, längste, zufälligen Weg wird manchmal oder gar nicht dargestellt
- A-Stern Algorthmus hinzugefügt


## [1.3.3] - 2018-12-11
### Added
- WIP: Implementierung A-Stern Algorithmus
- Start- und Zielpunkt sind nun über IV veränderbar


## [1.3.2] - 2018-12-06
### Added
- Refactoring: neue Klassen Dijkstra und einWeg hinzugefügt für die Algorithmen


## [1.3.1] - 2018-12-05
### Added
- WIP: Implementierung A-Stern Algorithmus
- Anzahl der Kanten unter den Knoten wurden erhöht
- Refactoring: neue Klasse Graph hinzugefügt zur Darstellung von Graphen


## [1.2.1] - 2018-12-04
### Added
- WIP: Implementierung A-Stern Algorithmus und Refactoring
- Darstellung der Knoten werden nun in erlaubten Flächen erstellt


## [1.1.0] - 2018-12-03
### Added
- WIP: Darstellung des Graphens
- Bug: die Berechnung der Position des Kantengewichtes ist noch fehlerhaft
- Darstellung der Knoten auf dem Bildschirm


## [1.0.0] - 2018-11-27
### Added
- WIP: noch keine Darstellung möglich
- Dijkstra und zufälligen Algorithmus hinzugefügt
- Grundgerüst für die Darstellung der Graphen
    - Kanten-, Knoten- und Main-Klassen wurden erstellt
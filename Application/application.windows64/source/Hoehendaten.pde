/**
 * Liest externe Höhendaten ein und stellt die Daten für weitere Nutzung zur Verfügung.
 */
public class Hoehendaten {

  public static final String KARTE_1 = "data/z1.txt";
  public static final String KARTE_2 = "data/z2.txt";
  public static final String KARTE_3 = "data/z3.txt";
  public static final String KARTE_4 = "data/z4.txt";
  public static final String KARTE_5 = "data/z5.txt";
  public static final String BIG_MAP = "data/zBig.txt";
  public static final float HOCHPUNKT = 100;
  public static final float TIEFPUNKT = 0;

  // aktuell gemappte Höhenwert
  private float[] hoehenwerte;
  private float[] urspungswerte;


  public float[] holeHoehenwerte() {
    return this.hoehenwerte;
  }


  /**
   * Lädt die Höhendaten ein.
   */
  public void ladeKarte(String karte) {
    this.hoehenWertAbspeichern(karte);
    this.mappeUrspungswerte();
  }

  /**
   * Lädt die Höhendaten aus einer externen Quelle und separiert die Werte nach Komma.
   * Die Daten werden als String abgespeichert.
   */
  private void hoehenWertAbspeichern(String karte) {
    BufferedReader bufReader = null;
    String gleitkommazahl = null;
    String[] hoehenwerteInString = null;

    try {
      //bufReader = new BufferedReader(new FileReader(dataPath("z1.txt")));
      bufReader = createReader(karte);
      while ((gleitkommazahl = bufReader.readLine()) != null) {
        hoehenwerteInString = split(gleitkommazahl, ",");
      }
      
      this.uebertrageStringInFloatArray(hoehenwerteInString);
    } catch (IOException e) {
      e.printStackTrace();
    } catch (NullPointerException e) {
      e.printStackTrace();
    } finally {
      try {
        if (bufReader != null) {
          bufReader.close();
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * Übertrage die Strings-Höhenwerte Indeces-weise in ein Float-Array.
   * Übertrüft vorher, ob die Werte valide sind.
   * @param werteInString Zuübergenes String-Array
   */
  private void uebertrageStringInFloatArray(String[] werteInString) {
    this.urspungswerte = new float[werteInString.length];

    for (int i = 0; i < werteInString.length; i++) {  
      // akzeptiertes Wort: einstellige Ziffer "\\d", gefolgt von einem Punkt [.], dann weitere Ziffer "\\d", ein oder mehr "+"
      if (werteInString[i].matches("\\d[.]\\d+")) {
        this.urspungswerte[i] = Float.parseFloat(werteInString[i]);
      } else {
        this.urspungswerte[i] = 0.0;
      }
    }
  }

  /**
   * Streckt die Ursprungshöhen in ein neues Intervall aus.
   */
  private void mappeUrspungswerte() {
    this.hoehenwerte = new float[this.urspungswerte.length];

    for (int i = 0; i < this.urspungswerte.length; i++) {
      this.hoehenwerte[i] = map(this.urspungswerte[i], this.minHoehe(this.urspungswerte), this.maxHoehe(this.urspungswerte), Hoehendaten.TIEFPUNKT, Hoehendaten.HOCHPUNKT);
    }
  }

  /**
   * Gib die Minimumhöhe aus.
   * @param eingangshoehe Ausgangsarray mit Höhenwerten
   */
  public float minHoehe(float[] eingangshoehe) {
    float minimum = Float.MAX_VALUE;

    for (int i = 0; i < eingangshoehe.length; i++) {
      if (eingangshoehe[i] < minimum) {
        minimum = eingangshoehe[i];
      }
    }
    return minimum;
  }

  /**
   * Gibt die Maximumhöhe aus.
   * @param eingangshoehe Ausgangsarray mit Höhenwerten
   */
  public float maxHoehe(float[] eingangshoehe) {
    float maximum = Float.MIN_VALUE;

    for (int i = 0; i < eingangshoehe.length; i++) {
      if (eingangshoehe[i] > maximum) {
        maximum = eingangshoehe[i];
      }
    }
    return maximum;
  }
} // Ende Klasse Hoehendaten

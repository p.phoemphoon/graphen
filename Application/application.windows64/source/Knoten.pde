/**
 * Die Klasse Knoten beinhaltet die Koordinaten und Heuristik der einzelnen Knoten.
 * Jede Knoten sollte seine Nachbar und seine Kantengewichte kennen.
 */
public class Knoten {

  // Eigenschaften der Klasse-Knoten
  private float xPosition;
  private float yPosition;
  private float zPosition;
  private float heuristik;

  private HashMap<Knoten, Float> kanten = new HashMap<Knoten, Float>();
  private ArrayList<Knoten> nachbarn = new ArrayList<Knoten>();


  public Knoten(float _xPos, float _yPos, float _zPos) {
    this.xPosition = _xPos;
    this.yPosition = _yPos;
    this.zPosition = _zPos;
  }

  public float xKoordinate() {
    return this.xPosition;
  }

  public float yKoordinate() {
    return this.yPosition;
  }
  
  public float zKoordinate() {
    return this.zPosition; 
  }

  public float wertDerHeuristik() {
    return this.heuristik;
  }

  public ArrayList<Knoten> listeMitNachbarn() {
    return this.nachbarn;
  }

  public void setzeHeuristik(float _heuristik) {
    this.heuristik = _heuristik;
  }


  public String toString() {
    System.out.println();
    return "\nKnoten: x: " + this.xPosition + " | y: " + this.yPosition + "  | z: " + this.zPosition;
  }

  /**
   * Liefert das Kantengewicht vom Start- zum Endknoten zurück.
   * Bsp.: Startknoten -> Endknoten (param: ende)
   @param ende Endknoten
   */
  public float holeKantengewicht(Knoten ende) {
    return this.kanten.get(ende);
  }

  /**
   * Ziel: Alle Knoten sollen seine Nachbarn kennen.
   * Erstellt die Kanten aus der bereits verfügbaren Knoten.
   * @param Startknoten
   * @param kantengewicht das dazugehörige Kantengewicht
   */
  private void erstelleKante(Knoten knoten, float kantengewicht) {
    this.kanten.put(knoten, kantengewicht);
    this.nachbarn.add(knoten);
  }
} // ENDE Klasse Knoten

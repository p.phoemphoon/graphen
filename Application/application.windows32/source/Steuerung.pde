/*
 *
 *                        Steuerung der Höhenkarte
 *
 */

// max. 2 (Start- und Zielknoten), um das Markieren weiterer Knoten und andere mögliche Bugs zu verhindern
private int anzahlAusgewaehlterKnoten = 0; 
// noch kein Start- und Zielknoten ausgewählt
private boolean startzustand = false;
private boolean zielzustand = false;
// Index des Startknotens der vorherigen Suche
private int indexVorherigerStartknoten = 0;
private int indexVorherigerEndknoten = 0;
// Ziffer die der Position des Knoten in "knoten" entspricht
private int knotenziffer = 0; 

/**
 * Steuerungsmethode Processing
 * wird nach dem Drücken der linken Maustaste ausgeführt
 * hier: Knotenauswahl
 * Startknoten: Knoten, von welchem aus sie Wegsuche beginnen soll
 * Endknoten: Knoten, welcher durch die Wegsuche erreicht werden soll
 */
void mousePressed() {
  // wenn sich der Mauszeiger in einem Kreis befindet 
  if (graph.mausInKreis()) {

    // Mehrfachauswahl; falls nötig eine erneute Wegsuche auf dem Graphen vorbereiten
    neueAuswahlErmoeglichen();

    // Knotenziffer zunächst wieder auf 0 setzen
    this.knotenziffer = 0;

    // Auswahl Startknoten
    startknotenAuswaehlen();

    // Auswahl Endknoten
    endknotenAuswaehlen();
  }

  // falls Start- und Zielknoten beide gesetzt sind 
  if (startzustand == true && zielzustand == true) {
    startzustand = false;
    zielzustand = false;
  }
}

/**
 * Belegt die Taste von 1-5 mit verschiedenen Karten.
 * [A] = A-Stern, [S] = Zufall, [D] = Dijkstra
 * mit [LEERTASTE] wird die Karte zurückgesetzt
 */
public void keyPressed() {
  switch (key) {
    case '1':
      this.resetAuswahl();
      graph.erzeugeGittergraph(Hoehendaten.KARTE_1);
    break;
    case '2':
      this.resetAuswahl();
      graph.erzeugeGittergraph(Hoehendaten.KARTE_2);
    break;
    case '3':
      this.resetAuswahl();
      graph.erzeugeGittergraph(Hoehendaten.KARTE_3);
    break;
    case '4':
      this.resetAuswahl();
      graph.erzeugeGittergraph(Hoehendaten.KARTE_4);
    break;
    case '5':
      this.resetAuswahl();
      graph.erzeugeGittergraph(Hoehendaten.KARTE_5);
    break;
    case '0':
      this.resetAuswahl();
      graph.erzeugeGittergraph(Hoehendaten.BIG_MAP);
    break;
    // Fälle für Algorithmen
    case 'a':
      if (anzahlAusgewaehlterKnoten == 2) {
        graph.astern();
      }
    break;
    case 's':
      if (anzahlAusgewaehlterKnoten == 2) {
        graph.zufaellig();
      }
    break;
    case 'd':
      if (anzahlAusgewaehlterKnoten == 2) {
        graph.dijkstra();
      }
    break;
    case ' ':
      this.resetAuswahl();
      background(45, 45, 52);
      graph.generiereGitterkanten();
      graph.zeichneGitterknoten();
    break;
    default:
      System.out.println("\n" +
                         "Die gedrückte Taste ist nicht belegt.\n" + 
                         "Drücke [1], [2], [3], [4] oder [5] für die Auswahl der Karte.\n" +
                         "Drücke [LEERTASTE] für Reset der Karte.\n" +
                         "[A]: AStern, [S]: Zuzfall, [D]: Dijsktra");
  }
}

/**
 * Bevor ein neuer Graph erstellt wird, Startknoten, Zielknoten und Anzahl wieder auf 0 setzen 
 */
private void resetAuswahl() {
  graph.setzeStartknoten(0);
  graph.setzeZielknoten(0);
  this.anzahlAusgewaehlterKnoten = 0;
  startzustand = false;
  zielzustand = false;
}

/**
 * Methode ermöglicht eine erneute Wegsuche auf dem selben Graphen.
 */
private void neueAuswahlErmoeglichen() {
  // falls die Anzahl der ausgewaehlten Knoten = 2 ist
  if (this.anzahlAusgewaehlterKnoten == 2) {
    // Knoten und Kanten erneut zeichnen
    background(45, 45, 52);
    graph.generiereGitterkanten();
    graph.zeichneGitterknoten();

    // Anzahl der ausgewaehlten Knoten wieder auf 0 setzen
    this.anzahlAusgewaehlterKnoten = 0;
  }
}

/**
 * Methode zum Setzen und Markieren des Startknotens
 */
private void startknotenAuswaehlen() {
  // falls noch kein Stratknoten ausgewählt wurde
  if (startzustand == false) {
    // über die Ziffern aller Knoten des Graphen iterieren
    for (int i = 0; i < graph.holeGitteranzahlLaenge(); i++) {
      // falls die X- und Y-Koordinaten des Kreises in dem sich die Maus beim Klicken befindet den Koordinaten des Knotens an der Stelle i in "knoten" entsprechen
      if (graph.holeKreisXKoordinate() == graph.holeKnoten().get(i).xKoordinate() && graph.holeKreisYKoordinate() == graph.holeKnoten().get(i).yKoordinate()) {
        // Knoten an Stelle [i] wird durch Kreis in welchem geklickt wurde beschrieben
        this.knotenziffer = i;
        this.indexVorherigerStartknoten = i;
      }
    }

    // setze den Knoten an der Stelle [i] als Startknoten
    graph.setzeStartknoten(knotenziffer);
    System.out.println("\n##################################\n" + 
                       "Stratknoten Nr.: " + graph.holeStartknoten());
    // Startknoten gesetzt/ausgewählt
    startzustand = true;
    // daher Anzahl ausgewaehlter Knoten erhöhen
    this.anzahlAusgewaehlterKnoten++;

    // Start-Markierung zeichnen
    fill(25, 130, 55); // grün
    strokeWeight(2);
    stroke(255);
    ellipse(graph.holeKreisXKoordinate(), graph.holeKreisYKoordinate(), Graph.RADIUS * 2, Graph.RADIUS * 2);
    strokeWeight(1);
  }
}

/**
 * Methode zum Setzen und Markieren des Endknotens
 */
private void endknotenAuswaehlen() {
  // falls ein Startknoten aber noch kein Zielknoten ausgewählt wurde
  if (startzustand == true && zielzustand == false) {
    // über die Ziffern aller Knoten des Graphen iterieren
    for (int i = 0; i < graph.holeGitteranzahlLaenge(); i++) {
      // falls die X- und Y-Koordinatne des Kreises in dem sich die Maus beim Klicken befindet den Koordinaten des Knotens an der Stelle i in "knoten" entsprechen
      if (graph.holeKreisXKoordinate() == graph.holeKnoten().get(i).xKoordinate() && graph.holeKreisYKoordinate() == graph.holeKnoten().get(i).yKoordinate()) {
        // Knoten an Stelle [i] wird durch Kreis in welchem geklickt wurde beschrieben
        this.knotenziffer = i;
        this.indexVorherigerEndknoten = i;
      }
    }

    // falls Knoten an Stelle [i] nicht schon als Startknoten gewählt wurde
    if (this.knotenziffer != graph.holeStartknoten()) {
      // setze Knoten an Stelle [i] als Zielknoten
      graph.setzeZielknoten(this.knotenziffer);
      println("Zielknoten Nr.: " + graph.holeZielknoten());
      // Zielknoten gesetzt/ausgewählt
      zielzustand = true;
      // daher Anzahl ausgewaehlter Knoten erhöhen
      this.anzahlAusgewaehlterKnoten++;

      // Ziel-Markierung zeichnen
      fill(255, 0, 20); // rot
      strokeWeight(2);
      stroke(255);
      ellipse(graph.holeKreisXKoordinate(), graph.holeKreisYKoordinate(), Graph.RADIUS * 2, Graph.RADIUS * 2);
      strokeWeight(1);
    }
  }
}
